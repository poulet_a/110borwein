#encoding: utf-8

gem 'gruff'
require 'gruff'

def get_bar resolution, title, xlabel, ylabel, values
  g = Gruff::Bar.new resolution
  g.title = title
  g.x_axis_label = xlabel
  g.y_axis_label = ylabel

  # values is a {rectangle: [value, %, time]}
  values.each do |k, v|
    g.data k, [ Math::PI / 2 - v[0], v[1] * 10]
  end

  g.labels = {
    0 => "diff (pi/2 - value)",
    1 => "time (ds)"
  }

  return g
end
