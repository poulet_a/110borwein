#encoding: utf-8

class PICalc

  def self.output r, m, precision
    puts "#{m}    \t:#{r.round precision}, soit une difference de #{(Math::PI / 2 - r).round precision}"
  end

  # x => div
  # n => nbr iterations
  def self.calc n, x
    r = 1.0
    k = 0.0
    if x == 0
      return r
    end
    while k <= n
      r = r * (Math.sin(x / (2*k + 1)) / (x / (2*k + 1)))
      k += 1
    end
    return r
  end

  def initialize options
    @n = options[:n]
    @x = [0, options[:xmax]]
    @divisions = options[:divisions]
    @precision = options[:precision]
    @h = (@x[0] - @x[1]) / @divisions
  end

  def rectangle
    PICalc.rectangle @n, @x, @h, @precision, @divisions
  end

  def trapeze
    PICalc.trapeze @n, @x, @h, @precision, @divisions
  end

  def simpson
    PICalc.simpson @n, @x, @h, @precision, @divisions
  end

  def gauss
    PICalc.gauss @n, @x, @h, @precision, @divisions
  end

  def self.rectangle n, x, h, precision, divisions
    t = Time.now
    r = 0.0
    i = 0.0
    while i < divisions
        r = r + calc(n, i * h)
        i += 1
    end

    r = r * h
    r = r.abs
    output(r, "rectangle", precision)
    return [r.to_f, Time.now - t]
  end

  def self.trapeze n, x, h, precision, divisions
    t = Time.now
    r = 0.0
    i = 1.0
    while i < divisions
      r = r + calc(n, i * h)
      i += 1
    end

    r = (2 * r + calc(n, x[0]) + calc(n, x[1])) * ((x[1] - x[0]) / (divisions * 2))
    r = r.abs
    output(r, "trapeze", precision)
    return [r.to_f, Time.now - t]
  end

  def self.simpson n, x, h, precision, divisions
    t = Time.now
    r1 = 0.0
    r2 = 0.0

    i = 1
    while i < divisions
      r1 = r1 + calc(n, i * h)
      i += 1
    end

    i = 0
    while i < divisions
      r2 = r2 + calc(n, i * h + h / 2)
      i += 1
    end

    r = (r1 * 2 + r2 * 4 + calc(n, x[0]) + calc(n, x[1])) * (x[1] - x[0]) / (divisions * (4 + 2))
    r = r.abs
    output(r, "simpson", precision)
    return [r.to_f, Time.now - t]
  end

  # https://fr.wikipedia.org/wiki/Calcul_int%C3%A9gral#M.C3.A9thode_de_Simpson
  def self.gauss n, x, h, precision, divisions
    t = Time.now
    r = 0.0
    i = 1.0
    di = (x[1] - x[0]) / 2
    de = (x[1] + x[0]) / 2
    while i < divisions
      r = r + calc(n, di * i * h + de)
      i += 1
    end

    r = di * r / divisions * 6
    r = r.abs
    output(r, "gauss", precision)
    return [r.to_f, Time.now - t]
  end
end
