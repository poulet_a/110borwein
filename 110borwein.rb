#!/usr/bin/env ruby
#encoding: utf-8

gem 'myerror'
require 'myerror'

require_relative 'options.rb'
require_relative 'graph.rb'
require_relative 'interact.rb'
require_relative 'picalc.class.rb'

def calculate options
  result = {}
  pi = PICalc.new(options)
  if options[:methods].include? 'rectangle'
    result[:rectangle] = pi.rectangle()
  end
  if options[:methods].include? 'trapeze'
    result[:trapeze] = pi.trapeze()
  end
  if options[:methods].include? 'simpson'
    result[:simpson] = pi.simpson()
  end
  if options[:methods].include? 'gauss'
    result[:gauss] = pi.gauss()
  end
  g = get_bar(options[:resolution], options[:title], "", "", result)
  output = "out." + options[:output]
  playerVerbose = '2>&1 > /dev/null'
  if options[:verbose] == true
    puts "drawing #{output}..."
    playerVerbose = ''
  end
  g.write(output)
  `mkdir -p serveur/app/assets/images/ app/assets/images/`
  `cp #{output} serveur/app/assets/images/`
  `cp #{output} app/assets/images/`
  if options[:display] == true
    trap("SIGINT"){exit 0}
    if options[:output].match /(jpg)|(png)/
      `eog #{output} #{playerVerbose}`
    elsif options[:output].match /(pdf)/
      `okular #{output} #{playerVerbose}`
    end
  end
  return [result, g]
end

def main argv
  options = read_argv(argv)
  if options[:interact] == true
    return interactif options
  end
  MyError::Error.call "n undefined" if options[:n] == false
  return calculate options
end
