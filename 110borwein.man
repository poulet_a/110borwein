.\" Manpage for 110borwein.
.\" Contact poulet_a@epitech.eu and broggi_t@epitech.eu in to correct errors or typos.
.TH 110BORWEIN "32" "April 2014" "1.0" "110borwein man page"
.SH NAME
.PP
110borwein \- calculate Borwein integrals using several numerical integration methods.
.SH SYNOPSIS
.PP
\fB./110borwein\fR [\fIOPTIONS\fR]... \fIn\fR
.SH DESCRIPTION
.PP
\fB./110borwein\fR is a program that calculate Borwein integrals approximations using several numerical integration methods. It calculates it with the \fIrectangle\fR method, the \fItrapeze\fR method, the \fISimpson\fR method and the \fIGauss\fR method. The given parameter is \fIn\fR. The output is the value calculated for each method, and the difference between this value and pi/2. It displays it on a graph. You can change a lot of parameters, given in the OPTIONS section of this manpage. If n is not defined, an interactive mode will be launched.
.SH OPTIONS
.TP
\fB--display\fR
The graph will be displayed. It is the default value.
.TP
\fB-d\fR, \fB--divisions\fR \fIINT\fR
Change the number of subdivisions. The default value is 10000.
.TP
\fB--gauss\fR
Calculate the integrale using the Gauss method. If none of \fI--gauss\fR, \fI--rectangle\fR, \fI--simpson\fR and \fI--trapeze\fR is specified, all methods will be used.
.TP
\fB-h\fR, \fB--help\fR
Display this help and exit.
.TP
\fB--info\fR
Display informations about variable in interactive mode.
.TP
\fB-i\fR, \fB--interactif\fR
Launch the interactive mode. The others given parameters will be used.
.TP
\fB--nodisplay\fR
The graph will not be display.
.TP
\fB--noinfo\fR
Do not display any informations in interactive mode.
.TP
\fB--noverbose\fR
Disable all of the output, excepted the results. It is the default value.
.TP
\fB-o\fR, \fB--out\fR, \fB--output\fR \fIFORMAT\fR
Change the output format of the image. Possibles values are \fIjpg\fR, \fIpng\fR, \fIpdf\fR. Default value is png.
.TP
\fB-p\fR, \fB--precision\fR \fIINT\fR
Change the precision of the estimations. It is the number of digits. Default value is 10.
.TP
\fB--prompt\fR \fISTRING\fR
Change the prompt in the interactive mode.
.TP
\fB--python\fR
Launch the python version of the program. It is faster, but less precise and without any options.
.TP
\fB--rectangle\fR
Calculate the integrale using the rectangle method. If none of \fI--gauss\fR, \fI--rectangle\fR, \fI--simpson\fR and \fI--trapeze\fR is specified, all methods will be used.
.TP
\fB-r\fR, \fB--resolution\fR \fIWIDTH\fRx\fIHEIGHT\fR
Change the resolution of the graph. Default value is 800x600.
.TP
\fB--ruby\fR
Launch the ruby version of the program. It is slower, but more precise and all options are working. It is the default value.
.TP
\fB--simpson\fR
Calculate the integrale using the Simpson method. If none of \fI--gauss\fR, \fI--rectangle\fR, \fI--simpson\fR and \fI--trapeze\fR is specified, all methods will be used.
.TP
\fB-t\fR, \fB--title\fR
Change the title of the graphical window.
.TP
\fB--trapeze\fR
Calculate the integrale using the trapeze method. If none of \fI--gauss\fR \fI--rectangle\fR, \fI--simpson\fR and \fI--trapeze\fR is specified, all methods will be used.
.TP
\fB-v\fR, \fB--verbose\fR
Explain what is being done.
.TP
\fB-x\fR, \fB--xmax\fR \fIINT\fR
Change the value of xmax.
.SH INTERACTIVE MODE
.PP
If \fB./110borwein\fR is run without \fIn\fR value, or with one of the \fI-i\fR or \fI--interactif\fR, an interactive mode is launched. Some of the \fB./110borwein\fR options are only woking in the interactive mode, as \fI--info\fR, \fI--noinfo\fR, \fI--prompt\fR. The interactive mode is only available in the ruby version of the program. So you need to launch it with the \fI--ruby\fR option if you want to use it. In interactive mode, you can only use one of the following commands :
.TP
\fBc\fR, \fBcalc\fR
Calculate the integrale with all of the four methods.
.TP
\fBdisplay\fR
The graph will be displayed.
.TP
\fBd\fR, \fBdivisions\fR \fIINT\fR
Change the number of subdivisions
.TP
\fBexit\fR
Exit the program.
.TP
\fBgauss\fR
Calculate the integrale with only the Gauss method.
.TP
\fBh\fR, \fBhelp\fR
Display all of the available commands.
.TP
\fBinfo\fR
Display informations about variable between each command.
.TP
\fBm\fR, \fBman\fR
Display this man and exit.
.TP
\fBn\fR \fIINT\fR
Change the value of n.
.TP
\fBnodisplay\fR
The graph will not be displayed.
.TP
\fBnoinfo\fR
Do not display informations.
.TP
\fBo\fR, \fBout\fR, \fBoutput\fR \fIFORMAT\fR
Change the output format of the image. Possibles values are \fIjpg\fR, \fIpng\fR, \fIpdf\R.
.TP
\fBp\fR, \fBprecision\fR \fIINT\fR
Change the precision of the estimations. It is the number of digits.
.TP
\fBprompt\fR \fISTRING\fR
Change the prompt.
.TP
\fBquit\fR
Exit the program.
.TP
\fBrectangle\fR
Calculate the integrale with only the rectangle method.
.TP
\fBr\fR, \fBrectangle\fR, \fIWIDTH\fRx\fIHEIGHT\fR
Change the resolution of the graph.
.TP
\fBsimpson\fR
Calculate the integrale with only the Simpson method.
.TP
\fBt\fR, \fBtitle\fR
Change the title of the graphical window.
.TP
\fBtrapeze\fR
Calculate the integrale with only the trapeze method.
.TP
\fBx\fR, \fBxmax\fR \fIINT\fR
Change the value of xmax.
.SH BONUS
.PP
We made two versions of this program : one in ruby, with a lot of options and one in python. We add a fourth calculation method: the gauss method. There is an interactive mode, with 21 commands made especially for this project. In this one, you can change the prompt command and thedisplay of current variables.  There is a man which explain all the project, and all the available options. We can do the calculations with only 1, 2 or 3 methods. If it is asked, a graphical window will pop after all the calculations, that allow us to compare easily all the methods. We can choose the resolution of the window, the output format and the title of the window. We can change the value of xmax, the number of subdivisons and the precision of the calculations. There is a web server version developped with the Ruby on Rails framework.
.SH EXAMPLES
.TP
\fB./110borwein\fR \fI0\fR
rectangle		:1.8207508757, soit une difference de -0.2499545489
.br
trapeze		:1.5707014773, soit une difference de 9.48495e-05
.br
simpson		:1.5706690971, soit une difference de 0.0001272297
.br
gauss		:1.497910954, soit une difference de 0.0728853728
.TP
\fB./110borwein\fR \fI0\fR \fB--xmax\fR \fI2000\fR
rectangle		:1.6700125452, soit une difference de -0.0992162184
.br
trapeze		:1.5700590472, soit une difference de 0.0007372796
.br
simpson		:1.5700722482, soit une difference de 0.0007240786
.br
gauss		:0.5936994675, soit une difference de 0.9770968593
.br
.TP
\fB./110borwein\fR \fI0\fR \fB-p\fR \fI2\fR
rectangle		:1.67, soit une difference de -0.1
.br
trapeze		:1.57, soit une difference de 0.0
.br
simpson		:1.57, soit une difference de 0.0
.br
gauss		:0.59, soit une difference de 0.98
.TP
\fB./110borwein\fR \fI0\fR \fB--gauss\fR \fB--rectangle\fR
rectangle		:1.8207508757, soit une difference de -0.2499545489
.br
gauss		:1.497910954, soit une difference de 0.0728853728
.TP
\fB./110borwein\fR \fI0\fR \fB--trapeze\fR \fB--precision\fR \fI5\fR \fB--nodisplay\fR
trapeze		:1.5707, soit une difference de 9.0e-05
.SH SEE ALSO
No related manpage.
.SH REPORTING BUGS
No known bugs.
.br
Report ./110borwein bugs to arthur.poulet@epitech.eu and thibaut.broggi@epitech.eu
.SH AUTHOR
poulet_a, broggi_t
