#!/usr/bin/env ruby
#encoding: utf-8

def read_argv a
  options = {
    n: false,
    xmax: 5000,
    divisions: 10000.0,
    precision: 10,
    resolution: "800x600",
    interact: false,
    display: true,
    verbose: false,
    output: "png",
    title: "graph",
    methods: [],
    prompt: ">",
    infos: false
    }

  i = 0
  while i < a.size do

    if a[i].match /(\-x)|(\-\-xmax)/ and a[i+1].to_i > 1
      i += 1
      options[:xmax] = a[i].to_i

    elsif a[i].match /(\-\-display)/
      options[:display] = true

    elsif
      a[i].match /(\-d)|(\-\-divisions)/ and a[i+1].to_f > 1
      i += 1
      options[:divisions] = a[i].to_f

    elsif a[i].match /(\-\-gauss)/
      options[:methods] << "gauss"

    elsif a[i].match /(\-h)|(\-\-help)/
      exec("man ./110borwein.man")

    elsif a[i].match /(\-\-info)/
      infos = true

    elsif a[i].match /(\-i)|(\-\-interactif)/
      options[:interact] = true

    elsif a[i].match /(\-\-nodisplay)/
      options[:display] = false

    elsif a[i].match /(\-\-noinfo)/
      infos = false

    elsif a[i].match /(\-\-noverbose)/
      options[:verbose] = false

    elsif a[i].match /(\-o)|(\-\-out)|(\-\-output)/ and a[i+1].match /(jpg)|(png)|(pdf)/
      i += 1
      options[:output] = a[i].to_s

    elsif a[i].match /(\-p)|(\-\-precision)/ and a[i+1].to_i > 1
      i += 1
      options[:precision] = a[i].to_i

    elsif a[i].match /(\-\-prompt)/
      i += 1;
      options[:prompt] = a[i].to_s

    elsif a[i].match /(\-\-rectangle)/
      options[:methods] << "rectangle"

    elsif a[i].match /\A(-r)|(--resolution)\Z/ and a[i+1].match /\A\d+x\d+\Z/
      i += 1
      options[:resolution] = a[i].to_s

    elsif a[i].match /(\-\-simpson)/
      options[:methods] << "simpson"

    elsif a[i].match /(\-\-trapeze)/
      options[:methods] << "trapeze"

    elsif a[i].match /(\-t)|(\-\-title)/
      i += 1
      options[:title] = a[i].to_s

    elsif a[i].match /(\-v)|(\-\-verbose)/
      options[:verbose] = true

    elsif a[i].match /\A\d+\Z/
      MyError::Error.call "n is negative. Absolute value will be used.", MyError::Error::ERR_LOW if a[i].to_i < 0
      options[:n] = a[i].to_i.abs
    end

    i += 1
  end

  if options[:methods].count == 0
    options[:methods].push "rectangle", "trapeze", "simpson", "gauss"
  end

  if options[:n] == false
    options[:interact] =  true
  end

  return options
end
